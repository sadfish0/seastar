#pragma once

template <typename T>
class iterator {
public:
	iterator() { m_ptr = nullptr; };
	iterator(T* ptr) :m_ptr(ptr) {}
	bool operator!=(const iterator& other) const;
	bool operator>=(const iterator& other) const;
	T operator*() const { return *m_ptr; }
	iterator& operator++();
	iterator& operator--();
	iterator operator ++(int);
	iterator operator --(int);

private:
	T* m_ptr;
};

template<typename T>
inline bool iterator<T>::operator!=(const iterator& other) const
{
	return this->m_ptr != other.m_ptr;
}

template<typename T>
inline bool iterator<T>::operator>=(const iterator& other) const
{
	return this->m_ptr >= other.m_ptr;
}

template<typename T>
inline iterator<T>& iterator<T>::operator++()
{
	 ++m_ptr;
	return *this;
}

template<typename T>
inline iterator<T>& iterator<T>::operator--()
{
	--m_ptr;
	return *this;
}

template<typename T>
inline iterator<T> iterator<T>::operator++(int)
{
	iterator tmp = *this;
	++m_ptr;
	return tmp;
}

template<typename T>
inline iterator<T> iterator<T>::operator--(int)
{
	iterator tmp = *this;
	--m_ptr;
	return tmp;
}
