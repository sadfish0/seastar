#include "tree.h"

TreeNode* Tree::create()
{
	TreeNode* Node = new TreeNode();
	char c;
	std::cin >> c;
	if ('#' == c) Node = nullptr;
	else {
		Node->val = c;
		Node->left = create();
		Node->right = create();
	}
	return Node;
}

void Tree::preorder(TreeNode* root)
{
	if (!root) return;
	pre.push_back(root->val);
	preorder(root->left);
	preorder(root->right);
}

void Tree::minorder(TreeNode* root)
{
	if (!root) return;
	minorder(root->left);
	mid.push_back(root->val);
	minorder(root->right);
}

void Tree::afterorder(TreeNode* root)
{
	if (!root) return;
	afterorder(root->left);
	afterorder(root->right);
	after.push_back(root->val);
}

void Tree::init()
{
	preorder(root);
	minorder(root);
	afterorder(root);
}
