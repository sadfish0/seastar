#pragma once
#include "vec.h"
#include "iterator.h"
#include "iteapre.h"
#include <iostream>

struct TreeNode {
	char val;
	TreeNode* left;
	TreeNode* right;
	TreeNode():val(0),left(nullptr),right(nullptr){}
	TreeNode(char _val) :val(_val),left(nullptr),right(nullptr){}
	TreeNode(char _val, TreeNode* _left, TreeNode* _right) :val(_val), left(_left), right(_right){}
};

class Tree {
public:
	using iteapre = TreeIteratorp<TreeNode*>;
	Tree() { root = nullptr; }
	TreeNode* create();
	iterator <char> pbegin() { return iterator<char> (pre.begin()); }
	iterator <char> pend() {return iterator<char>(pre.end());}
	iterator <char> mbegin() { return iterator<char>(mid.begin()); }
	iterator <char> mend() { return iterator<char>(mid.end()); }
	iterator <char> abegin() { return iterator<char>(after.begin()); }
	iterator <char> aend() { return iterator<char>(after.end()); }
	iteapre begin() { return iteapre(root); }
	void preorder(TreeNode * root);
	void minorder(TreeNode * root);
	void afterorder(TreeNode* root);
	void init();
	TreeNode* root;

private:
	
	vec<char> pre;
	vec<char> mid;
	vec<char> after;
};
