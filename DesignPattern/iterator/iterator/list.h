#pragma once
template <class T>
struct Node {
	Node* pre;
	Node* next;
	T value;
	Node() { pre = nullptr; next = nullptr; }
	Node(T _value) :value(_value) {pre = nullptr;next = nullptr;}
	Node(T _value,Node* _pre,Node* _next):value(_value),pre(_pre),next(_next){}
};

template <class T>
class List {
public:
	using NodePtr = Node<T>*;
	List();
	void push_back(T _value);                     // 在链表末尾插入一个值为_value的节点
	void insert(T __value,T _value);              // 在值为__value节点后边插入一个 值为_value为节点
	void erase(T _value);                         // 删除值为_value的节点
	int getSize() { return length; }
	NodePtr begin() { return pre->next; }        //  链表头
	NodePtr _end() { return end; }               //  链表尾
	NodePtr _pre() { return pre; }
private:
	NodePtr pre;                   //头节点前一个节点
	NodePtr end;                   //尾节点
	int length = 0;                //链表长度
};

template<class T>
inline List<T>::List()
{
	pre = new Node<T>();
	pre->next = end = nullptr;
}

template<class T>
inline void List<T>::push_back(T _value)
{
	NodePtr node = new Node<T>(_value);
	if (!end) {
		pre->next = node;
		node->next = nullptr;
		node->pre = this->pre;
		end = node;
		length++;
		return;
	}
	node->pre = end;
	node->next = nullptr;
	end->next = node;
	end = end->next;
	length++;
}

template<class T>
inline void List<T>::insert(T __value, T _value)
{
	auto it = pre->next;
	for (; it && it->value != __value; it = it->next);
	if (!it) {
		std::cout << "没有该节点!" << "\n";
		return;
	}
	NodePtr node = new Node<T>(_value);
	node->pre = it;
	node->next = it->next;
	it->next = node;
	if (node->next == nullptr) {
		end = node;
		length++;
		return;
	}
	node->next->pre = node;
	length++;
}

template<class T>
inline void List<T>::erase(T _value)
{
	auto it = pre->next;
	for (; it && it->value != _value; it=it->next);

	if (!it) {
		std::cout << "没有该节点!\n";
		return;
	}

	if (it == pre->next) {
		NodePtr tmp= pre->next;
		pre->next = pre->next->next;
		delete tmp;
	}
	else if(it==end) {
		NodePtr tmp = it;
		end = it->pre;
		it->pre->next = nullptr;
		delete it;
	}
	else {
		NodePtr tmp = it;
		it->pre->next = it->next;
		it->next->pre = it->pre;
		delete it;
	}

	length--;
}
