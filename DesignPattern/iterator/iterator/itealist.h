#pragma once
#include "list.h"

template<class T>

class ListIterator {
public:
	using NodePtr = Node<T>*;
	ListIterator() { node = nullptr; }
	ListIterator(NodePtr _node) { node = _node; }
	bool operator!=(const ListIterator& other) const;
	T operator*() const { return node->value; }
	//ListIterator& operator++();
	ListIterator operator ++(int);
	ListIterator operator --(int);
private:
	NodePtr node;
};

template<class T>
inline bool ListIterator<T>::operator!=(const ListIterator& other) const
{
	return this->node != other.node;
}

/*template<class T>
inline ListIterator<T>& ListIterator<T>::operator++()
{
	node = node->next;
	return *this;
}*/

template<typename T>
inline ListIterator<T> ListIterator<T>::operator++(int)
{
	ListIterator tmp = *this;
	node = node->next;
	return tmp;
}

template<class T>
inline ListIterator<T> ListIterator<T>::operator--(int)
{
	ListIterator tmp = *this;
	node = node->pre;
	return tmp;
}
