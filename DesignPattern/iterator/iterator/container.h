#pragma once
#include "iterator.h"
#include <algorithm>
#include <vector>
#include <math.h>

template <class T>
class container {
public:
	using iterator = iterator<T>;
	container(){}
	container(int size, T num);
	container(std::initializer_list<T> lst);
	virtual iterator begin() { return iterator(m_ptr); }
	virtual iterator end() { return iterator(m_ptr + _size); }
	virtual ~container() { delete[]m_ptr; }
	virtual int  find_first_of(T num)=0;
	virtual int find_last_of(T num)=0;
	virtual void push_back(T num){};
	virtual void pop_back(){};
	int getSize() { return _size; }
	void setSize(int size);
protected:
	int _size= 0;
	T _num=0;
	T* m_ptr;
};


template<class T>
inline container<T>::container(int size, T num)
{
	_size = size;
	_num = num;
	m_ptr = new T[size];
	for (int i = 0; i < size; i++) *(m_ptr + i) = num;
}

template<class T>
inline container<T>::container(std::initializer_list<T> lst)
{
	_size = lst.size();
	m_ptr = new T[lst.size()]{};
	std::copy(lst.begin(), lst.end(), m_ptr);
}

template<class T>
void container<T>::setSize(int size)
{
	//delete[]m_ptr;
	T* ptr = new T[size];
	for (int i = 0; i < std::min(this->_size,size); i++) {
		ptr[i] = m_ptr[i];
	}
	delete[] m_ptr;
	m_ptr = ptr;

	this->_size = size;
}