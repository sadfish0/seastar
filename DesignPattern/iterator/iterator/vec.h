#pragma once
#include "container.h"
template <class T>
class vec : public container<T>{
public:
	vec(){}
	vec(int size, T num);
	vec(std::initializer_list<T> lst);
	T& operator[](int i) { return this->m_ptr[i]; }
	T at(int i) { return this->m_ptr[i]; }
	int  find_first_of(T num) override;
	int find_last_of(T num)override;
	void push_back(T num)override ;
	void pop_back() override ;
	// int getSize() override;
};


template<class T>
vec<T>::vec(int size, T num)
{
	this->_size = size;
	this->_num = num;
	this->m_ptr = new T[size];
	for (int i = 0; i < size; i++) *(this->m_ptr + i) = num;
}

template<class T>
vec<T>::vec(std::initializer_list<T> lst)
{
	this->_size = (int )lst.size();
	this->m_ptr = new T[lst.size()]{};
	std::copy(lst.begin(), lst.end(), this->m_ptr);
}

template<class T>
inline int vec<T>::find_first_of(T num)
{
	for (int i = 0; i < this->getSize(); i++) {
		if (*(this->m_ptr + i) == num) return i;
	}
	return -1;
}

template<class T>
int vec<T>::find_last_of(T num)
{
	for (int i = this->getSize() - 1; i >= 0; i--) {
		if (this->m_ptr[i] == num) return i;
	}
	return -1;
}

template<class T>
void vec<T>::push_back(T num)
{
	this->setSize(this->getSize() + 1);
	this->m_ptr[this->getSize() - 1] = num;
}

template<class T>
void vec<T>::pop_back()
{
	this->_size--;
}


