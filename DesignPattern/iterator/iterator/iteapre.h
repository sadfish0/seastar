#pragma once
#include "iterator.h"
#include "tree.h"
#include <stack>

template<class T>
class TreeIteratorp :public iterator<T> {
public:
	TreeIteratorp(T root) {cur = root; }
	bool operator!=(const TreeIteratorp& other) const;
	char operator*() const { return cur->val; }
	TreeIteratorp& operator++();
private:
	T cur;
	std::stack<T> s;
};

template<class T>
inline bool TreeIteratorp<T>::operator!=(const TreeIteratorp& other) const
{
	return this->cur != other.cur;
}

template<class T>
inline TreeIteratorp<T>& TreeIteratorp<T>::operator++()
{
	if (cur) {
		if (cur->right) s.push(cur->right);
		if (cur->left) s.push(cur->left);
	}

	if (!s.empty())
	{
		cur = s.top();
		s.pop();
	}
	else
	{
		cur = nullptr;
	}

	return *this;
}
