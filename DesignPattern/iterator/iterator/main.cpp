#include<iostream>
#include "container.h"
#include "vec.h"
#include "tree.h"
#include "iteapre.h"
#include "itealist.h"
int main()
{
	/*for (auto i : container<int>(1, 6)) {
		std::cout << i << " ";
	}std::cout << "\n";

	container<int> vec(4, 5);
	iterator<int> it;
	for (it = vec.begin(); it != vec.end(); it++) {
		std::cout << *it << " ";
	}*/

	std::cout << "查找元素:\n";
	vec<int > vec1(2, 3);
	std::cout<<vec1.at(0)<<" ";
	std::cout<<vec1[1]<<"\n";

	vec<int > vec2{ 1,2,3,4,5,2};
	iterator<int> it;
	std::cout << "遍历:\n";
	for (it = vec2.begin(); it != vec2.end(); it++) {
		std::cout << *it << " ";
	}std::cout << "\n";
	--it;
	for (; it >=vec2.begin(); it--) {
		std::cout << *it << " ";
	}std::cout << "\n";
	std::cout << "查找元素第一次和最后一次出现的位置:\n";
	std::cout<<vec2.find_first_of(0)<<" ";
	std::cout<<vec2.find_last_of(2)<<"\n";
	std::cout << "删除元素后size:\n";
	vec2.pop_back();
	std::cout << vec2.getSize() << "\n";
	std::cout << "末尾插入后size:" << "\n";
	vec2.push_back(6);
	std::cout << vec2.getSize() << "\n";
	std::cout << "遍历:\n";
	for (auto it : vec2) {
		std::cout << it << " ";
	}std::cout << "\n\n";

	const type_info &type=typeid(vec1.begin());
	std::cout << type.name() << "\n\n";


	std::cout << "按照前序构建一棵二叉树(#为空):\n";
	Tree* tree=new Tree();
	tree->root = tree->create();
	tree->init();

	iterator <char> iter;
	for (iter = tree->pbegin(); iter != tree->pend(); iter++) {      
		std::cout << *iter << " ";
	}std::cout << "\n";
	for (iter = tree->mbegin(); iter != tree->mend(); iter++) {
		std::cout << *iter << " ";
	}std::cout << "\n";
	for (iter = tree->abegin(); iter != tree->aend(); iter++) {
		std::cout << *iter << " ";
	}std::cout << "\n\n";
	
	std::cout << "前序遍历迭代器:\n";
	auto itea = tree->begin();
	for (; itea != nullptr; ++itea) {
		std::cout << *itea << " ";
	}std::cout << "\n\n";


	ListIterator<int >  lit;
	List<int> list;
	list.push_back(1);
	list.push_back(2);
	list.insert(2, 3);
	list.push_back(4);
	list.erase(3);
	list.erase(5);
	list.insert(7, 6);
	std::cout << "链表的遍历:\n";
	for (lit = list.begin(); lit !=nullptr; lit++) {
		std::cout << *lit << " ";
	}std::cout << "\n";
	std::cout << list.getSize() << "\n";

	for (lit=list._end(); lit!=list._pre(); lit--) {
		std::cout << *lit << " ";
	}std::cout << "\n";


	delete tree;
	return 0;
}