### 状态模式

代码逻辑如下所示，唯一的区别是增加了Super,Fire,Cape马里奥收到攻击会变为small马里奥，而samll马里奥收到攻击会死亡。

![](../image/飞书20220726-172932.jpg)



- Mario                                      马里奥的基类

- Singleton                               单例模式，获得当前状态引用  

- MarioState                            马里奥状态转移

- SuperMario                          获得蘑菇变成超级马里奥

- FireMario                              获得花变成火焰马里奥

- CapeMario                            获得羽毛变成飞行马里奥

  如果需要扩展其他状态添加对应的状态类即可

  

通过如下代码获得类名，从而获得当前的状态：

```c++
  virtual std::string __className() { \
    std::string nameBuffer( __FUNCTION__ ); \
    std::smatch result; \
    std::regex pattern( "(\\w+)::" ); \
    std::regex_search( nameBuffer, result, pattern ); \
    return result[1]; \
  }
```

game：游戏运行的类。有length,width属性控制对应的窗口大小，以及对应修改的函数，可以修改窗口的长和宽。使用pair代表当前马里奥的位置，使用vector<pair<int,int >>来存储蘑菇，花，羽毛，攻击物的位置。因此扩展时可以通过修改vector来删除或者增加马里奥遇到的物体。

运行结果如下：

![](../image/飞书20220726-175822.jpg)

![](../image/飞书20220726-175826.jpg)

![](../image/飞书20220726-175829.jpg)

![](../image/飞书20220726-175832.jpg)

![](../image/飞书20220726-181127.jpg)