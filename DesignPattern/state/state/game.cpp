#include "game.h"
#include <Windows.h>
#include <iostream>
#include <unordered_set>
#include <conio.h>
#include <algorithm>
game::game() {
	mario = std::make_shared<Mario>();
	mario_m.first = width-1;
	mario_m.second = 1;
	Mushroom.push_back(std::make_pair(width-1,10));
	FireFlower.push_back(std::make_pair(width - 1, 18));
	Feather.push_back(std::make_pair(width - 2, 30));
	Attack.push_back(std::make_pair(width-1,45));
	Attack.push_back(std::make_pair(width - 2, 60));
}

void game::MoveLeft() {
	if (mario_m.second== 1) {
		std::cout << "不能左移了!" << "\n";
		return;
	}
	mario_m.second--;
	system("cls");
	draw();
}

void game::MoveRight() {
	if (mario_m.second == length) {
		std::cout << "恭喜过关!" << "\n";
		exit(0);
	}
	mario_m.second++;
	system("cls");
	draw();
}

void game::MoveUp() {
	mario_m.first--;
	system("cls");
	draw();
	eat();
	Sleep(500);
	mario_m.first++;
	system("cls");
	draw();
}

bool game::Contain(std::vector<std::pair<int, int >> tmp, std::pair<int, int> t) {
	for (auto it : tmp) {
		if (it.first == t.first && it.second == t.second) return true;
	}return false;
}

void game::draw() {
	for (int i = 0; i <=width; i++) {
		for (int j = 0; j <=length; j++) {
			if (i == width) { std::cout << "="; continue; }
			if (j == 0) { std::cout << "#"; }
			if (i == 0) { std::cout << "="; continue; }
			//if (j == length) { std::cout << "#"; }

			if (mario_m.first == i && mario_m.second == j) {
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_BLUE);
				std::cout << "i";
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | \
					FOREGROUND_GREEN | FOREGROUND_BLUE);
			}
			else if (Contain(Mushroom, std::make_pair(i, j))) {
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_GREEN);
				std::cout << "蘑";
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | \
					FOREGROUND_GREEN | FOREGROUND_BLUE); 
			}
			else if (Contain(FireFlower, std::make_pair(i, j))) {
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED);
				std::cout << "花";
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | \
					FOREGROUND_GREEN | FOREGROUND_BLUE);
			}
			else if (Contain(Feather, std::make_pair(i, j))) {
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | \
					FOREGROUND_BLUE);
				std::cout << "羽";
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | \
					FOREGROUND_GREEN | FOREGROUND_BLUE);
			}
			else if (Contain(Attack, std::make_pair(i, j))) {
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_GREEN | \
					FOREGROUND_BLUE);
				std::cout << "危";
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | \
					FOREGROUND_GREEN | FOREGROUND_BLUE);
			}
			else {
				std::cout << " ";
			}
		}std::cout << "\n";
	}
}

void game::eat() {
	if (Contain(Mushroom, std::make_pair(mario_m.first,mario_m.second))) {
		mario->GotMushroom();
		Mushroom.erase(remove(Mushroom.begin(), Mushroom.end(), std::make_pair(mario_m.first, mario_m.second)));
		system("cls");
		draw();
	}
	else if (Contain(FireFlower, std::make_pair(mario_m.first, mario_m.second))) {
		mario->GotFireFlower();
		FireFlower.erase(remove(FireFlower.begin(), FireFlower.end(), std::make_pair(mario_m.first, mario_m.second)));
		system("cls");
		draw();
		//std::cout << "现在的状态是:" << mario->GetName() << "\n";
		//std::cout << "现在的金币是:"; mario->ReportCoin();
	}
	else if (Contain(Feather, std::make_pair(mario_m.first, mario_m.second))) {
		mario->GotFeather();
		Feather.erase(remove(Feather.begin(), Feather.end(), std::make_pair(mario_m.first, mario_m.second)));
		system("cls");
		draw();
		//std::cout << "现在的状态是:" << mario->GetName() << "\n";
		//std::cout << "现在的金币是:"; mario->ReportCoin();
	}
	else if (Contain(Attack, std::make_pair(mario_m.first, mario_m.second))) {
		mario->GotAttack();
		Attack.erase(remove(Attack.begin(), Attack.end(), std::make_pair(mario_m.first, mario_m.second)));
		system("cls");
		draw();
		//std::cout << "现在的状态是:" << mario->GetName() << "\n";
		//std::cout << "现在的金币是:"; mario->ReportCoin();
	}
}

void game::play() {
	std::cout << "输入移动方向:w(上),a(左),d(右),q(退出):\n";
	std::unordered_set<char > move;
	move.insert('w');
	move.insert('a');
	move.insert('d');
	move.insert('q');
	char c;
	c = _getch();
	while (!move.count(c)) {
		std::cout << "没有该命令，请重新输入:\n";
		c = _getch();
	}
	
	while (c=_getch()) {
		if (c == 'q') {
			mario->ReportCoin();
			return;
		}
		else if (c == 'w') {
			MoveUp();
			eat();
		}
		else if (c == 'a') {
			MoveLeft();
			eat();
		}
		else if (c == 'd') {
			MoveRight();
			eat();
		}
		std::cout << "现在的状态是:" << mario->GetName() << "\n";
		std::cout << "现在的金币是:"; mario->ReportCoin();
	}
}

void game::SetLength(int _length)
{
	length = _length;
}

void game::SetWidth(int _width)
{
	width = _width;
}

int game::GetLength()
{
	return length;
}

int game::GetWidth()
{
	return width;
}
