#pragma once
template<typename T>

class Singleton {
public:
	static T& GetInstance();

	Singleton(T&&) = delete;
	Singleton(const T&) = delete;
	void operator=(const T&) = delete;

protected:
	Singleton() = default;
	virtual ~Singleton() = default;

};

template<typename T>
inline T& Singleton<T>::GetInstance()
{
	static T instance;
	return instance;
	// TODO: �ڴ˴����� return ���
}
