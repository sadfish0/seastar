#pragma once
#include "Mario.h"
#include "Singleton.h"

class CapeMario :public MarioState, public Singleton<CapeMario> {
public:
	void GotMushroom(Mario* mario);
	void GotFireFlower(Mario* mario);
	void GotFeather(Mario* mario);
	void GotAttack(Mario* mario) override;
	CLASS_INFO;
};