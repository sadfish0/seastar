#include <iostream>
#include <memory>
#include "Mario.h"
#include "SuperMario.h"
#include "game.h"
#include <Windows.h>
int main()
{
	std::unique_ptr<Mario> mario=std::make_unique<Mario>();

	std::cout << "小马里奥获得蘑菇:" << "\n";
	mario->GotMushroom();
	std::cout<<mario->GetName()<<"\n";
	std::cout << "获得花:" << "\n";
	mario->GotFireFlower();
	std::cout << mario->GetName() << "\n";
	std::cout << "获得羽毛:" << "\n";
	mario->GotFeather();
	std::cout << mario->GetName() << "\n";
	mario->ReportCoin();
	std::cout << "受到攻击:" << "\n";
	mario->GotAttack();
	std::cout<<mario->GetName()<<"\n";
	//std::cout << "受到攻击:" << "\n";
	//mario->GotAttack();
	std::cout << "\n\n";

	std::cout << "开始游戏:" << "\n";
	Sleep(1000);
	system("cls");

	std::shared_ptr<game> g = std::make_shared<game>();
	g->draw();
	g->play();
	return 0;


}