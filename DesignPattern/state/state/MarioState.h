#pragma once
#include <regex>
#define CLASS_INFO \
  virtual std::string __className() { \
    std::string nameBuffer( __FUNCTION__ ); \
    std::smatch result; \
    std::regex pattern( "(\\w+)::" ); \
    std::regex_search( nameBuffer, result, pattern ); \
    return result[1]; \
  }
class Mario;
class MarioState {
public:
	virtual void GotMushroom(Mario* mario) = 0;
	virtual void GotFireFlower(Mario* mario) = 0;
	virtual void GotFeather(Mario* mario) = 0;
    virtual void GotAttack(Mario* mario) = 0;
    virtual ~MarioState(){}
    CLASS_INFO;
    std::string GetName() { return __className(); }
};