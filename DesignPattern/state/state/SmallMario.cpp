#include "SmallMario.h"
#include <iostream>
void SmallMario::GotMushroom(Mario* mario) {
	mario->AddCoin(100);
	mario->SetState(&SuperMario::GetInstance());
}

void SmallMario::GotFireFlower(Mario* mario) {
	mario->AddCoin(200);
	mario->SetState(&FireMario::GetInstance());
}

void SmallMario::GotFeather(Mario* mario) {
	mario->AddCoin(300);
	mario->SetState(&CapeMario::GetInstance());
}

void SmallMario::GotAttack(Mario* mario) {
	std::cout << "you are dead!\n";
	exit(0);
}