#include "FireMario.h"

void FireMario::GotMushroom(Mario* mario) {
	mario->AddCoin(100);
}

void FireMario::GotFireFlower(Mario* mario) {
	mario->AddCoin(100);
}

void FireMario::GotFeather(Mario* mario) {
	mario->AddCoin(200);
	mario->SetState(&CapeMario::GetInstance());
}

void FireMario::GotAttack(Mario* mario) {
	mario->SetState(&SmallMario::GetInstance());
}