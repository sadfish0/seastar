#pragma once
#include "Mario.h"
#include "Singleton.h"
#include "FireMario.h"
#include "CapeMario.h"
#include "SuperMario.h"
class SmallMario : public MarioState, public Singleton<SmallMario> {
public:
	void GotMushroom(Mario* mario);
	void GotFireFlower(Mario* mario);
	void GotFeather(Mario* mario);
	void GotAttack(Mario* mario);
	CLASS_INFO;
};
