#pragma once
#include <iostream>
#include <memory>
#include "Mario.h"
#include "SuperMario.h"
#include <utility>
#include <vector>
#include <unordered_set>
class game {
public:
	game();
	void MoveLeft();
	void MoveRight();
	void MoveUp();
	void play();
	void draw();
	void SetLength(int _length);
	void SetWidth(int _width);
	int GetLength();
	int GetWidth();
private:
	int length=100;
	int width=20;
	std::shared_ptr<Mario>  mario;
	std::pair<int, int > mario_m;                           //马里奥坐标
	std::vector<std::pair<int, int >> Mushroom;      //蘑菇坐标
	std::vector<std::pair<int, int >> FireFlower;    //花的坐标
	std::vector<std::pair<int, int >> Feather;        //羽毛的坐标
	std::vector<std::pair<int, int >> Attack;         //可以攻击马里奥的坐标
	bool Contain(std::vector<std::pair<int, int >> tmp, std::pair<int, int> t);
	void eat();
};
