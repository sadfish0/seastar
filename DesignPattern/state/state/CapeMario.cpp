#include "CapeMario.h"
#include "FireMario.h"

void CapeMario::GotMushroom(Mario* mario) {
	mario->AddCoin(100);
}

void CapeMario::GotFireFlower(Mario* mario) {
	mario->AddCoin(200);
	mario->SetState(&FireMario::GetInstance());
}

void CapeMario::GotFeather(Mario* mario) {
	mario->AddCoin(200);
}

void CapeMario::GotAttack(Mario* mario) {
	mario->SetState(&SmallMario::GetInstance());
}