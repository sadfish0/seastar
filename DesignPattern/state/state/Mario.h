#pragma once
#include "MarioState.h"
#include "SmallMario.h"
#include <string>


class Mario {
private:
	MarioState* state;
	int coin;
	
public:
	Mario();
	void SetState(MarioState* state);
	void AddCoin(int numberOfCoins);
	void ReportCoin();
	void GotMushroom();
	void GotFireFlower();
	void GotFeather();
	void GotAttack();
	std::string GetName() { return state->GetName(); }
	virtual ~Mario(){}
};