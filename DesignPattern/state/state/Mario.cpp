#include "Mario.h"
#include <iostream>
#include <string>
Mario::Mario()
{
	coin = 0;
	this->state = &SmallMario::GetInstance();
}

void Mario::SetState(MarioState* state) {
	this->state = state;
	
}

void Mario::AddCoin(int numberOfCoins) {
	std::cout << "Got Coins:" << numberOfCoins << "\n";
	coin += numberOfCoins;
}

void Mario::ReportCoin() {
	std::cout << "Total Coin:" << coin << "\n";
}

void Mario::GotMushroom() {
	this->state->GotMushroom(this);
}

void Mario::GotFireFlower() {
	this->state->GotFireFlower(this);
}

void Mario::GotFeather() {
	this->state->GotFeather(this);
}

void Mario::GotAttack() {
	this->state->GotAttack(this);
}