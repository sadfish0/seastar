#pragma once
#include "Mario.h"
#include "Singleton.h"
#include "FireMario.h"
#include "CapeMario.h"
class SuperMario :public MarioState, public Singleton<SuperMario> {
public:
	void GotMushroom(Mario* mario);
	void GotFireFlower(Mario* mario);
	void GotFeather(Mario* mario);
	void GotAttack(Mario* mario);
	CLASS_INFO;
};
