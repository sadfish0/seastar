#include "SuperMario.h"
void SuperMario::GotMushroom(Mario* mario) {
	mario->AddCoin(50);
}

void SuperMario::GotFireFlower(Mario* mario) {
	mario->AddCoin(300);
	mario->SetState(&FireMario::GetInstance());
}

void SuperMario::GotFeather(Mario* mario) {
	mario->AddCoin(500);
	mario->SetState(&CapeMario:: GetInstance());
}

void SuperMario::GotAttack(Mario* mario) {
	mario->SetState(&SmallMario::GetInstance());
}