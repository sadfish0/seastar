#pragma once
#include "Mario.h"
#include "Singleton.h"
#include "CapeMario.h"
class FireMario :public MarioState, public Singleton<FireMario> {
public:
	void GotMushroom(Mario* mario);
	void GotFireFlower(Mario* mario);
	void GotFeather(Mario* mario);
	void GotAttack(Mario* mario);
	CLASS_INFO;
};
